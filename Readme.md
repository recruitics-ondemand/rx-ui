# Recruitics UI
This repo contains a few shared React components and stylesheets for Recruitics projects.  

# Notes
For now, this is more-or-less spike code and its API is not finalized.  Use at your own risk.

# Development
Branch definitions

* `master` - stable production branch  
    * `hotfix/...` - branched from `master` and merged to both `master` and `develop`
* `develop` - development branch
    * `feature/...` - branched from `develop` and merged back
    * `bugfix/...` - branched from `develop` and merged back
    * `release/...` - branched from `develop` and merged to both `master` and `develop` (tag `master` with new version number)