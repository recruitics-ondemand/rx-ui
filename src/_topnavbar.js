import React from 'react';
import $     from 'jquery';
var {
  Navbar,
  NavBrand,
  Nav,
  NavItem,
  NavDropdown,
  ButtonGroup,
  Button,
  DropdownButton,
  MenuItem,
  SplitButton
} = require('react-bootstrap');

class TopNavbar extends React.Component {

  constructor (props) {
    super(props);

    this.state = {
      selectedVendor: null,
      toggleableSidebar: false
    };
  }

  companySelectHandler (ev) {
    if(this.props.companySelectHandler) {
      this.props.companySelectHandler(ev);
    }
  }

  render() {
    var rightDropdownItems = this.props.rightDropdownItems;
    var leftDropdownItems = this.props.leftDropdownItems;
    var vendor = this.state.selectedVendor;
    return (
      <Navbar
        className="topnavbar"
        role="navigation"
        fluid
        bsSize="small"
        bsStyle="success">
        <NavBrand left>
          {
              this.props.projectLogo
          }
          {
            this.props.projectName &&
              <span>{ this.props.projectName}</span>
          }
        </NavBrand>
        <Nav right className='nav-pills'>
        {
          this.props.user && this.props.user.admin &&
            <li
                className='vendor-select-dropdown-nav-item'
                role='presentation'
              >
              <SplitButton
                id='vendor-select-dropdown'
                title={
                  vendor ? vendor.name : (
                    _.isEmpty(leftDropdownItems) ? 'No vendors mapped' : 'Select a vendor'
                  )
                }
                right
                eventKey={ vendor ? vendor.id : null }
                onClick={ this.props.user && this.props.user.admin ? this.vendorClickHandler : undefined }
                onSelect={ this.props.user && this.props.user.admin ? this.vendorSelectHandler : undefined }
                >
                {
                  leftDropdownItems && leftDropdownItems.map((item, idx) => {
                    return (
                      <MenuItem eventKey={ idx } key={ idx }>
                          { item.name }
                      </MenuItem>
                    );
                  })
                }
              </SplitButton>
            </li>
          }
          <select className={ 'chosen-select input-sm pull-left' } onChange={ this.companySelectHandler.bind(this) } value={ this.props.selectedCompany && this.props.selectedCompany.companyid } >
            <option>
              Select a company
            </option>
            {
              rightDropdownItems && rightDropdownItems.map((item, idx) => {
                return (
                  <option
                    key={ idx }
                    value={ item.value }
                  >
                    { item.text }
                  </option>
                );
              })
            }
          </select>
          {
            this.state.toggleableSidebar &&
              <NavItem eventKey={0} href='#' onClick={this.props.parent ? this.props.parent.toggleSidebar : undefined }>
                <div className="fa fa-navicon fa-lg navbar-icon">&nbsp;</div>
              </NavItem>
          }
          <NavItem eventKey={1} href='#' onClick={ this.props.parent ? this.props.parent.toggleRightSidebar : this.props.toggleRightSidebar }>
            <div className="fa fa-user fa-lg navbar-icon">&nbsp;</div>
          </NavItem>
        </Nav>
      </Navbar>
    );
  }
}

TopNavbar.defaultProps = {
  rightDropdownItems: [],
  leftDropdownItems: []
};

export default TopNavbar;
