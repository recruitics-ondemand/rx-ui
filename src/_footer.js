import React from 'react';

class Footer extends React.Component {
  render () {
    return (
        <span className='rx-footer'>
          &copy; 2016 - Recruitics, LLC
        </span>
      );
  }
}

export default Footer;
