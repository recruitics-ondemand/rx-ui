import React from 'react';
import { Button, ButtonGroup, Grid, Row, Col } from 'react-bootstrap';
import TopNavbar from './_topnavbar';
import Sidebar from './_sidebar';
import Footer from './_footer';
import Alert from './_alert';

const nullPageComponent = <div className='rx-main-panel'>&nbsp;</div>;

// Example dropdown and menu items
const sidebarItems = [
  {
    href: "#",
    name: "Item 1",
    action: "toggle"
  },
  {
    href: "#",
    name: "Item 2",
    action: "toggle"
  },
  {
    href: "#",
    name: "Item 3",
    action: "toggle",
    children: [
      {
        name: "Item 3a",
        href: "#",
        action: "toggle"
      }
    ]
  }
];

const dropdownItems = [
  {
    name: 'one',
    href: '#'
  },
  {
    name: 'two',
    href: '#'
  },
  {
    name: 'three',
    href: '#'
  }
];

class Layout extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      expanded: false,
      offsidebarCollapsed: true
    };
  }

  componentWillReceiveProps() {

  }

  signOutHandler () {
    $.ajax({
      type: 'DELETE',
      data: {authenticity_token: $("meta[name='csrf-token']").attr('content')},
      url: '/users/sign_out'
    }).done(function () {
      window.location.replace("");
    });
  }

  render () {
    var projectLogo = this.props.projectLogoComponent || (<span className='rx-project-name'>{this.props.projectName}</span>);
    var isExpanded = this.state.expanded;
    // this.state.user && ... <UserPanel user={ this.state.user } changes={ this.state.changes }/>
    return (
      <div className='main-layout'>
        <Grid className='outer-grid' fluid>
          <Row fill className="topnavbar-wrapper">
            {
              this.props.navbarComponent ||
              <TopNavbar
                parent={this}
                ref='topNavBar'
                mdOffset={0}
                md={12}
                projectName={this.props.projectName}
                projectLogo={ projectLogo }
                leftDropdownItems={ this.props.leftDropdownItems || [] }
                rightDropdownItems={ this.props.rightDropdownItems || [] }
                />
            }
          </Row>
          <Row fill className="main-panel">
            <aside className="aside">
              {
                this.props.sidebarComponent ||
                <Sidebar ref='sidebar' parent={this} items={this.props.sidebarItems}/>
              }
            </aside>
            <section className={ "content-wrapper" + (isExpanded ? " expanded" : "" ) }>
              { this.props.pageComponent }
              <Footer/>
            </section>
          </Row>
        </Grid>
      </div>
    );
  }
}

Layout.defaultProps = {
  pageComponent: nullPageComponent,
  sidebarItems: sidebarItems,
  dropdownItems: dropdownItems
};

export default Layout;
