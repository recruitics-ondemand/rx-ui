import React from 'react';
import { Col } from 'react-bootstrap';
import FontAwesome from 'react-fontawesome';
import * as _ from 'lodash';

class Sidebar extends React.Component {

  constructor (props) {
    super(props);
    this.state = {
      items: props.items
    };
  }

  componentWillReceiveProps (props) {
    // Do stuff with props
    if (props && props.eventHandlers) {
      Object.keys(props.eventHandlers).map((e) => {
        var fn = props.eventHandlers[e];
        return props.eventHandlers[e] = fn.bind(this);
      });
    }
    this.setState({
      items: props.items
    });
  }

  updateItems (newSidebarItems) {
    this.setState({items: newSidebarItems});
  }

  setupItemEventHandler (item) {
    var handler = this.props.eventHandlers[item.action];
    if (!handler) return;
    return (evt) => {
      return handler(item, evt);
    };
  }

  render () {
    var isClosed = this.props.closed;
    var buildRightLabel = (item) => {
      var label = item.rightLabel;
      if (label) {
        return (
          <div className={`label label-${label.status || 'success'} pull-right`}>{label.text}</div>
        );
      } else {
        return null;
      }
    };
    var buildLineItem = (item, idx) => {
      return  (
        <li className={"nav-link" + (item.active ? " active" : "") + (isClosed ? " closed" : "" )} key={ idx }>
          <a href={ item.href || undefined } id={ item.id || undefined} onClick={ this.setupItemEventHandler(item) }>
            { buildRightLabel(item) }
            <em className={ (item.iconClass || "") + ( isClosed ? " closed-icon" : "" ) }></em>
            {
              !isClosed && <span>{ item.name || "" }</span>
            }
          </a>
          {
            !_.isEmpty(item.children) && !item.collasped ?
              <ul className="nav sidebar-subnav">
                { item.children.map(buildLineItem) }
              </ul> : null
          }
        </li>
      );
    };

    return (
        <div className="aside-inner">
          <nav className={ "sidebar" + (this.state.closed ? " closed" : "" ) } data-sidebar-anyclick-close="">
            <ul className="nav">
              <li className="nav-heading">
                <span></span>
              </li>
              { (() => this.state.items.map(buildLineItem))() }
            </ul>
          </nav>
        </div>
    );
  }
}

Sidebar.defaultProps = {
  items: [
    {
      name: 'Toggle',
      icon: 'fa chevron',
      href: '#'
    }
  ],
  eventHandlers: {},
  closed: false
};

export default Sidebar;
