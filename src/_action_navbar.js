var {
  Navbar,
  NavBrand,
  Nav,
  NavItem,
  NavDropdown,
  ButtonGroup,
  Button,
  DropdownButton,
  MenuItem,
  SplitButton
} = require('react-bootstrap');

var ActionNavbar = React.createClass({

  __chosenifyDropdowns: function () {
    var rightDropdownNode = React.findDOMNode(this.refs.rightDropdown);
    if (!global && $.fn.chosen) {
      $(rightDropdownNode).chosen();
    }
  },

  componentDidMount: function () {
    this.__chosenifyDropdowns();
  },

  componentDidUpdate: function () {
    this.__chosenifyDropdowns();
  },

  getInitialState: function () {
    return {
      selectedVendor: null
    };
  },

  getDefaultProps: function () {
    return {
      rightDropdownItems: [],
      leftDropdownItems: []
    };
  },

  vendorSelectHandler: function (ev, key) {
    var vendor = this.props.leftDropdownItems[key];
    vendor && this.setState({ selectedVendor: vendor });
  },

  vendorClickHandler: function (ev, key) {
  },

  render: function () {
    var companies = this.props.rightDropdownItems;
    var vendors = this.props.leftDropdownItems;
    var vendor = this.state.selectedVendor;
    return (
      <Navbar
        className="topnavbar"
        role="navigation"
        fluid
        bsSize="small"
        bsStyle="success">
        <NavBrand left>
          <span className="brand-logo">
            <img src="/assets/logo.png" alt="Action Logo" className="img-responsive" />
          </span>
        </NavBrand>
        <Nav right className='nav-pills'>
          <li
            ref='vendorSelectNavItem'
            className='vendor-select-dropdown-nav-item'
            role='presentation'
          >
            <SplitButton
              id='vendor-select-dropdown'
              title={
                vendor ? vendor.name : (
                  _.isEmpty(vendors) ? 'No vendors mapped' : 'Select a vendor'
                )
              }
              ref='leftDropdown'
              right
              eventKey={ vendor ? vendor.id : null }
              onClick={ this.vendorClickHandler }
              onSelect={ this.vendorSelectHandler }
              >
              {
                vendors.map((item, idx) => {
                  return (
                    <MenuItem eventKey={ idx } key={ idx }>
                        { item.name }
                    </MenuItem>
                  );
                })
              }
            </SplitButton>
          </li>
          <NavItem>
            <select
              className='chosen-select input-sm'
              ref='rightDropdown'>
              {
                companies.map((item, idx) => {
                  return (
                    <option key={ idx }
                      value={ item[1] }
                    >
                      { item[0] }
                    </option>
                  );
                })
              }
            </select>
          </NavItem>
          <NavItem eventKey={0} href='#' onClick={this.props.parent.toggleSidebar }>
            <div className="fa fa-navicon fa-lg navbar-icon">&nbsp;</div>
          </NavItem>
          <NavItem eventKey={1} href='#' onClick={this.props.parent.toggleRightSidebar }>
            <div className="fa fa-user fa-lg navbar-icon">&nbsp;</div>
          </NavItem>
        </Nav>
      </Navbar>
    );
  }
});

module.exports = ActionNavbar;
